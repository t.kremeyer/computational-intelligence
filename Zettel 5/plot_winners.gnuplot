set terminal png
set output 'out/winnerNeurons.png'
set ylabel 'y'
set xlabel 'x'
set grid
plot 'out/datapoints.csv' using 2:3 with points lc rgb 'red' pt 2 ps 0.7 title 'Datenpunkte', \
'out/winnerNeurons.csv' using 1:2 with points lc rgb '#007700' pt 5 ps 1.5 title 'Datenpunkte 100,...,1000', \
'out/winnerNeurons.csv' using 3:4 with points lc rgb 'blue' pt 7 ps 1.5 title 'Entspr. SOM Neuronen'
