#pragma once

#include <vector>

class SOMNeuron
{
public:
	SOMNeuron(const std::vector<double>& initialPosition);
	double quadEuclidDistance(const std::vector<double>& point) const;
	void neighbourLearning(const std::vector<double>& point, double learningRate, double weight);
	const std::vector<double>& getPosition() const;
private:
	void correctVectorSize(const std::vector<double>& v) const;
	std::vector<double> m_position;
};
