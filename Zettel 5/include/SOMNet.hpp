#pragma once

#include "../include/SOMNeuron.hpp"
#include <vector>

class SOMNet1D
{
public:
	SOMNet1D(const std::vector<std::vector<double>>& initialPositions);
	const std::vector<SOMNeuron>& getNeurons() const;
	const SOMNeuron &getWinnerNeuron(const std::vector<double> &point) const;
	void trainNet(const std::vector<double> &point, double learningRate, double sigma);

private:
	unsigned getWinnerNeuronIndex(const std::vector<double> &point) const;
	static double gauss(double d, double sigma);

	std::vector<SOMNeuron> m_neurons;
};
