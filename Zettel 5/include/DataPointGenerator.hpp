#pragma once

#include <random>

class DataPointGenerator
{
public:
	DataPointGenerator();
	std::vector<double> operator()(unsigned v);
private:
	std::mt19937 m_gen;
	std::normal_distribution<> m_dx;
	std::normal_distribution<> m_dy;
};
