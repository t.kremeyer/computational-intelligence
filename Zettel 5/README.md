Programm, was eine SOM darstellt und damit einige Tests erlaubt.

## Kompilieren: 
make all (C++17)

## Ausf�hrung:
`./run.sh [learningExampleCount = 10000] [somDumpInterval = 50] [learningRate = 0.3] [sigma = 10] [learningRateFactor = 0.9999] [sigmaFactor = 0.9999]`

* L�scht alle Daten im Ordner `out/`
* F�hrt das Programm mit den entspr. Parametern aus
* Erstellt Plots

## Abh�ngigkeiten: 
* `cowsay` f�r die Skripte :P
* `egrep` f�rs Dateien raussuchen
* `gnuplot` f�rs Plotten

## Funktion: 
Das Programm stellt eine SOM dar, die mit zuf�lligen Punkten aus den gegebenen Punkten initialisiert wird. Diese SOM lernt mit `learningExampleCount` zuf�llig ausgew�hlten Punkten. Dabei wird je nach dem Lernen von `somDumpInterval` Punkten eine CSV-Datei mit den SOM-Positionen ausgegeben.  
Das Programm f�hrt dazu Nachbarschaftslernen durch. Dabei legt es als Reichweitenfunktion die Gau�-Verteilung mit Parameter `sigma` zugrunde, wobei nach jeder Iteration `sigma *= sigmaFactor` ausgef�hrt wird, `sigma` also f�r ein sinnvoll gew�hltes `sigmaFactor` kleiner wird, um eine Konvergenz herbeizuf�hren. Die Lernrate `learningRate` wird aus dem Grund ebenfalls pro Iteration mit dem `learningRateFactor` multipliziert.

## Aufbau Ausgabeverzeichnis
* `datapoints.csv`: Die gegebenen Datenpunkte
* `winnerNeurons.csv`: Spalten 1, 2: X- und Y-Werte der Datenpunkte 100,200,...,1000; Spalten 3, 4: X- und Y-Werte der zugeh�rigen Winner-Units im untrainierten Netz
* `winnerNeurons.png`: Visualisierung der Winner-Units.
* `somPositions/csv`: Rohdaten der SOM-Neuron-Positionen f�r die entspr. Iteration
* `somPositions/png`: Visualisierung der SOM-Neuron-Positionen f�r die entspr. Iteration
* `iterations.gif`: Eine sch�nere Darstellung der Neuron-Positionen als GIF. Beachtet werden nur die CSV-Dateien, die "50.", "00.", "init." oder "trained." enthalten

## Skripte:
* `clear_outdir.sh`: Leert das `out`-Verzeichnis und bereitet es auf den n�chsten Programmlauf vor
* `plot_all.sh`: Erzeugt alle Plots
* `run.sh`: L�sst das Programm laufen
* `run_experiment.sh`: F�hrt das Experiment mit variierenden Werten f�r sigma, sigmaFactor, learningRate, learningRateFactor durch und l�dt die Ergebnisse in den Ordner test. Vorsicht: Dauert lange.