#!/bin/bash

cowsay "Make program ..."

make

learningRates=( 0.01 0.1 0.3 )
sigmas=( 1 5 10 )
learningRateFactors=( 0.999 0.9999 1 )
sigmaFactors=( 0.999 0.9999 1 )

for learningRate in "${learningRates[@]}"
do
	for sigma in "${sigmas[@]}"
	do
		for learningRateFactor in "${learningRateFactors[@]}"
		do
			for sigmaFactor in "${sigmaFactors[@]}"
			do
				cowsay "Running for values $learningRate, $sigma, $learningRateFactor, $sigmaFactor"
				
				./run.sh 10000 50 "$learningRate" "$sigma" "$learningRateFactor" "$sigmaFactor"
				
				mkdir -p test/LR${learningRate}/S${sigma}/LRF${learningRateFactor}/SF${sigmaFactor}
				
				mv out/* test/LR${learningRate}/S${sigma}/LRF${learningRateFactor}/SF${sigmaFactor}/
			done
		done
	done
done
