#include "../include/SOMNet.hpp"

#include <algorithm>
#include <cmath>

using namespace std;

SOMNet1D::SOMNet1D(const vector<vector<double>>& initialPositions)
{
	for(const vector<double> &position : initialPositions)
	{
		m_neurons.push_back(SOMNeuron(position));
	}
}

const vector<SOMNeuron>& SOMNet1D::getNeurons() const
{
	return m_neurons;
}

const SOMNeuron& SOMNet1D::getWinnerNeuron(const vector<double> &point) const
{
	return m_neurons[getWinnerNeuronIndex(point)];
}

void SOMNet1D::trainNet(const std::vector<double> &point, double learningRate, double sigma)
{
	unsigned winnerIndex = getWinnerNeuronIndex(point);

	for(unsigned i = 0; i < m_neurons.size(); i++)
	{
		SOMNeuron &neuron = m_neurons[i];
		neuron.neighbourLearning(point, learningRate, gauss(static_cast<signed>(winnerIndex)-i, sigma));
	}
}

unsigned SOMNet1D::getWinnerNeuronIndex(const vector<double> &point) const
{
	unsigned winnerIndex = -1;
	double winnerDistance = numeric_limits<double>::infinity();

	for(unsigned i = 0; i < m_neurons.size(); i++)
	{
		const SOMNeuron& neuron = m_neurons[i];

		double dist = neuron.quadEuclidDistance(point);
		if(winnerDistance > dist)
		{
			winnerDistance = dist;
			winnerIndex = i;
		}
	}

	return winnerIndex;
}


double SOMNet1D::gauss(double d, double sigma)
{
	return exp(-(d*d)/(2*sigma*sigma));
}
