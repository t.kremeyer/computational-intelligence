#include "../include/SOMNeuron.hpp"

using namespace std;

SOMNeuron::SOMNeuron(const vector<double>& initialPosition)
{
	for(unsigned i = 0; i < initialPosition.size(); i++)
	{
		m_position = std::vector<double>(initialPosition);
	}
}

double SOMNeuron::quadEuclidDistance(const vector<double>& point) const
{
	correctVectorSize(point);
	double res = 0;

	for(unsigned i = 0; i < m_position.size(); i++)
	{
		res += (point[i]-m_position[i])*(point[i]-m_position[i]);
	}

	return res;
}

void SOMNeuron::neighbourLearning(const vector<double>& point, double learningRate, double weight)
{
	correctVectorSize(point);

	for(unsigned i = 0; i < m_position.size(); i++)
	{
		m_position[i] += learningRate * (point[i] - m_position[i]) * weight;
	}
}

const vector<double>& SOMNeuron::getPosition() const
{
	return m_position;
}

void SOMNeuron::correctVectorSize(const vector<double>& v) const
{
	if(v.size() != m_position.size()) throw "SOMNeuron: Input vector size wrong";
}
