#include "../include/DataPointGenerator.hpp"
#include <random>
#include <iostream>
#include <cmath>

using namespace std;

DataPointGenerator::DataPointGenerator()
{
	random_device rd;
	m_gen = mt19937(rd());

	m_dx = normal_distribution<> (0,0.1);
	m_dy = normal_distribution<> (0,0.15);
}

//For the sake of simpler comparability use unsigned v as identifier of the point
//instead of double u (u = v * 0.02)
vector<double> DataPointGenerator::operator()(unsigned v)
{
	double u = v * 0.02;

	//TODO: Nur Parameterset neu erzeugen
	m_dx = normal_distribution<> (0,0.1*u);
	m_dy = normal_distribution<> (0,0.15*u);


	return std::vector<double>
	{
		2*(3+sqrt(u))*sin(u)+m_dx(m_gen),
		3*(3+sqrt(u))*cos(u)+m_dy(m_gen)
	};
}
