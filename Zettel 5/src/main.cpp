#include "../include/DataPointGenerator.hpp"
#include "../include/SOMNet.hpp"
#include "../include/SOMNeuron.hpp"

#include <map>
#include <vector>
#include <iostream>
#include <fstream>

using namespace std;

void generateDatapoints(vector<vector<double>> &points, unsigned begin, unsigned count)
{
	points.clear();

	DataPointGenerator gen;

	for(unsigned v = begin; v < begin+count; v++)
	{
		points.push_back(gen(v));
	}
}

void printDatapoints(const vector<vector<double>> &points, ostream& outstream = std::cout)
{
	outstream << "u\tx\ty" << endl;
	for(unsigned i = 0; i < points.size(); i++)
	{
		const auto &point = points[i];
		outstream << i*0.02 << "\t" << point[0] << "\t" << point[1] << endl;
	}
}

vector<double> &getRandomDatapoint(vector<vector<double>> &points)
{
	return points[rand() % points.size()];
}

//cf. http://stackoverflow.com/a/3722437
//Params:
// vMin: minimum possible value for v (inclusive)
// vCount: how many values for v are possible (vMin + vCount = vMax, exclusive)
// chooseCount: how many v-vals are to be chosen
void chooseVValues(vector<unsigned>& vVals, unsigned vMin, unsigned vCount, unsigned chooseCount)
{
	if(vCount < chooseCount) throw "maxIndex < count";

	vVals.clear();

	for(unsigned i = 0; i < chooseCount; i++)
	{
		//                  align       random
		unsigned nextIndex = vMin + (rand() % (vCount - i));
		for(unsigned value : vVals)
		{
			if(value <= nextIndex) nextIndex++;
		}
		vVals.push_back(nextIndex);
	}
}

void makeInitialPositions(vector<vector<double>>& initialPositions, vector<vector<double>>& datapoints, vector<unsigned>& vVals)
{
	initialPositions.clear();

	for(unsigned v : vVals)
	{
		initialPositions.push_back(vector<double>(datapoints[v]));
	}
}

void printSOMPositions(SOMNet1D& somNet, ostream& outstream = std::cout)
{
	const vector<SOMNeuron>& somNeurons = somNet.getNeurons();

	for(const SOMNeuron& somNeuron : somNeurons)
	{
		const vector<double> position = somNeuron.getPosition();
		for(double positionElement : somNeuron.getPosition())
		{
			outstream << positionElement << "\t";
		}
		outstream << endl;
	}
}

void trainNet(SOMNet1D& net, vector<vector<double>> &datapoints, unsigned exampleCount, double learningRate = 0.3, double sigma = 5.0, double learningRateFactor = 1.0, double sigmaFactor = 1.0, unsigned somDumpInterval = 20)
{
	for(unsigned i = 0; i < exampleCount; i++)
	{
		net.trainNet(getRandomDatapoint(datapoints), learningRate, sigma);

		if(!(i%somDumpInterval))
		{
			ofstream outfile;
			outfile.open("out/somPositions/csv/iteration" + to_string(i) + ".csv");
			printSOMPositions(net, outfile);
			outfile.close();
		}

		learningRate *= learningRateFactor;
		sigma *= sigmaFactor;
	}
}

void printWinnerNeurons(SOMNet1D& net, vector<vector<double>> &datapoints, ostream& outstream = std::cout)
{
	for(unsigned i = 100; i <= 1000 && i < datapoints.size(); i+=100)
	{
		const vector<double> &winnerPosition = net.getWinnerNeuron(datapoints[i]).getPosition();
		outstream << datapoints[i][0] << "\t" << datapoints[i][1] << "\t" << winnerPosition[0] << "\t" << winnerPosition[1] << endl;
	}
}

int main(int argc, char *argv[])
{
	unsigned learningExampleCount = argc > 1 ? atoi(argv[1]) : 10000;
	unsigned somDumpInterval = argc > 2 ? atoi(argv[2]) : 50;
	double learningRate = argc > 3 ? atof(argv[3]) : 0.3;
	double sigma = argc > 4 ? atof(argv[4]) : 10;
	double learningRateFactor = argc > 5 ? atof(argv[5]) : 0.9999;
	double sigmaFactor = argc > 6 ? atof(argv[6]) : 0.9999;

	vector<vector<double>> datapoints;
	generateDatapoints(datapoints, 0, 1001);

	ofstream outfile;

	outfile.open("out/datapoints.csv");
	printDatapoints(datapoints, outfile);
	outfile.close();

	vector<unsigned> vValues;
	chooseVValues(vValues, 0, 1001, 100);

	vector<vector<double>> initialPositions;
	makeInitialPositions(initialPositions, datapoints, vValues);

	SOMNet1D somNet(initialPositions);

	outfile.open("out/winnerNeurons.csv");
	printWinnerNeurons(somNet, datapoints, outfile);
	outfile.close();

	outfile.open("out/somPositions/csv/init.csv");
	printSOMPositions(somNet, outfile);
	outfile.close();

	trainNet(somNet, datapoints, learningExampleCount, learningRate, sigma, learningRateFactor, sigmaFactor, somDumpInterval);

	outfile.open("out/somPositions/csv/trained.csv");
	printSOMPositions(somNet, outfile);
	outfile.close();

}
