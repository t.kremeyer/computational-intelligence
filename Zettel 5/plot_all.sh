#!/bin/bash

mkdir -p out/somPositions/png

cowsay "Plotting training process ..."

for FILE in out/somPositions/csv/*
do
	NAME=${FILE##*/}
	BASE=${NAME%.csv}
	
	gnuplot -c plot_datapoints.gnuplot $BASE $BASE
done

cowsay "Plotting winner neurons ..."

gnuplot plot_winners.gnuplot

cowsay "Creating gif ..."

convert -delay 1x10 -loop 0 `ls -v out/somPositions/png/*.png | egrep -E "(([50]0)|init|trained)\."` out/somPositions/iterations.gif

cowsay "Done ☺"
