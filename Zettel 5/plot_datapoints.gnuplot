set terminal png
set output 'out/somPositions/png/$1.png'
set ylabel 'y'
set xlabel 'x'
set grid
plot 'out/datapoints.csv' using 2:3 with points lc rgb 'red' pt 2 ps 0.7 title 'Datenpunkte', \
'out/somPositions/csv/$0.csv' using 1:2 with linespoints lc rgb 'blue' lt 16 pt 4 ps 1 title 'SOM Neuronen'
