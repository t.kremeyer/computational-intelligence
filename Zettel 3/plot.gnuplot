set terminal png
set output 'untrained.png'
set ylabel 'y'
set xlabel 'x'
set grid
plot 'untrained_data.txt' using 1:2 with lines lt 1 title "Ausgabe Netz", 'untrained_data.txt' using 1:3 with lines lt 2 title "Echter Funktionswert", \
 'untrained_data.txt' using 1:4 with lines lt 3 title "Error^2"
