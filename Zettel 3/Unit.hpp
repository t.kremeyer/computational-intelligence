#pragma once

#include <functional>
#include <vector>
#include <cmath>

using namespace std;

class Unit
{
	protected:
		unsigned int inputCount;
		std::vector<double> weights;
	public:
	
		Unit(unsigned int iCount, std::vector<double> w) : inputCount(iCount), weights(w)
		{
			if(w.size() != iCount) throw "Error while creating Unit: Wrong weight count";
		}
		
		virtual double integrate(std::vector<double> input) = 0;
		
		virtual double transfer(double input) = 0;
		
		void setWeight(unsigned int number, double weight)
		{
			if(number < inputCount) weights[number] = weight;
			else throw "Error: Tried to assign non-existing weight (too large weigth number)";
		}
		
		void addToWeight(unsigned int number, double weight)
		{
			if(number < inputCount) 
			{
				weights[number] += weight;
			}
			else throw "Error: Tried to assign non-existing weight (too large weigth number)";
		}
		
		void setWeights(std::vector<double> w) 
		{
			if(w.size() != inputCount) throw "Error: Tried to assign weight vector that doesnt match the input count";
		}
		
		double getWeight(unsigned int number)
		{
			if(number < inputCount) return weights[number];
			else throw "Error: Tried to read non-existing weight (too large weight number)";
		}
		
		std::vector<double> getWeights()
		{
			return weights;
		}
};

class FermiUnit : public Unit
{
	public:
		FermiUnit(unsigned int iCount, std::vector<double> w) : Unit(iCount, w) {}
		
		double integrate(std::vector<double> input)
		{
			double output = 0;
			
			if(input.size() != inputCount) throw "Error: Wrong input vector size when integrating";
			for(int i = 0; i < input.size(); i++)
			{
				output += input[i] * weights[i];
			}
			
			return output;
		}
	
		double transfer(double input)
		{
			return 1.0/(1+exp(0-input));
		}
};

class LinearUnit : public Unit
{
	public:
		LinearUnit(unsigned int iCount, std::vector<double> w) : Unit(iCount, w){}
		
		double integrate(std::vector<double> input)
		{
			double output = 0;
			
			if(input.size() != inputCount) throw "Error: Wrong input vector size when integrating";
			for(int i = 0; i < input.size(); i++)
			{
				output += input[i] * weights[i];
			}
			
			return output;
		}
		
		double transfer(double input)
		{
			return input;
		}
};

class ConstantUnit : public Unit
{	
	private:
		double output;
	public:
		ConstantUnit(double o) : Unit(0, std::vector<double>()), output(o){}
		
		double integrate(std::vector<double> input)
		{
			return output;
		}
		
		double transfer(double input)
		{
			return output;
		}
		
		void setOutput(double o)
		{
			output = o;
		}
};

class BiasUnit : public ConstantUnit
{	
	public:
		BiasUnit() : ConstantUnit(1){}
};
