#pragma once

#include <vector>
#include <cmath>
#include <string>

#include "Unit.hpp"



class PerceptronLayer
{
	private:
		std::vector<Unit*> units;
	
	public:
		PerceptronLayer(std::vector<Unit*> u) : units(u){}
	
		std::vector<double> getResult(std::vector<double> input)
		{
			std::vector<double> results;
			
			for(auto unit : units)
			{
				results.push_back(unit->transfer(unit->integrate(input)));
			}
			
			return results;
		}
		
		std::vector<double> getIntegratedValues(std::vector<double> input)
		{
			std::vector<double> results;
			
			for(auto unit : units)
			{
				results.push_back(unit->integrate(input));
			}
			
			return results;
		}
		
		std::vector<double> getTransferValues(std::vector<double> input)
		{
			if(input.size() != units.size()) throw "Error @PerceptronLayer.getTransferValues(): input-size " + std::to_string(input.size()) + " mismatches unit-count " + std::to_string(units.size());
			std::vector<double> results;
			
			for(int i = 0; i < units.size(); i++)
			{
				results.push_back(units[i]->transfer(input[i]));
			}
			
			return results;
		}
		
		void addUnit(Unit *unit)
		{
			units.push_back(unit);
		}
		
		int getUnitCount()
		{
			return units.size();
		}
};
