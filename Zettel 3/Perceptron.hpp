#pragma once

#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include <algorithm>

#include "Unit.hpp"
#include "PerceptronLayer.hpp"

using namespace std;

class ExamplePerceptron
{
	private:
		std::vector<Unit*> hiddenUnits;
		std::vector<Unit*> outputUnits;
		
		PerceptronLayer *l_hid;
		PerceptronLayer *l_out;
		
		//cf. http://stackoverflow.com/a/3722437
		list<double> chooseRandomXValues(int n)
		{
			list<double> picked;
			for(int i = 0; i < n; i++)
			{
				//rand() % (1001-i) => int between 0 and 1000 (999, 998, ...)
				//((20.0/1000) * (rand() % (1001-i))) => double between 0.0 and 20.0 (19.98, 19.96, ...)
				//((20.0/1000) * (rand() % (1001-i))) - 10 => double between -10.0 and 10.0 (9.98, 9.96, ...)
				
				//int randVal;
				//double nextVal;
				//cout << "rand() = " << (randVal = rand()) << endl;
				//cout << "% (1001-i) = " << (nextVal = randVal % (1001-i)) << endl;
				//cout << "(20.0/1000) * = " << (nextVal = (20.0/1000) * nextVal) << endl;
				//cout << "-10 = " << (nextVal = nextVal - 10) << endl;
				double nextVal = ((20.0/1000) * (rand() % (1001-i))) - 10;
				for(auto value : picked)
				{
					if(value <= nextVal) nextVal+=0.02;
				}
				picked.push_back(nextVal);
			}
			
			return picked;
		}
	
	public:
		void init()
		{	
			srand(time(0));
			
			//Init Netz
			std::vector<double> weights;
			
			for(int i = 0; i < 11; i++)
			{
				weights.push_back((0.5 - (-0.5)) * ( (double)rand() / (double)RAND_MAX ) + (-0.5));
			}
			
			cout << "Output weights: ";
			for_each(weights.begin(), weights.end(), [](double weight){cout << weight << ", ";});
			cout << endl;
			
			Unit *outputUnit = new LinearUnit(11, weights);
			outputUnits.push_back(outputUnit);
			
			cout << "Hidden weights: ";
			for(int i = 0; i < 10; i++)
			{
				std::vector<double> weights;
				weights.push_back((0.5 - (-0.5)) * ( (double)rand() / (double)RAND_MAX ) + (-0.5));
				weights.push_back((0.5 - (-0.5)) * ( (double)rand() / (double)RAND_MAX ) + (-0.5));
				cout << "P" << i << ": " << weights[0] << ", " << weights[1] << endl;
				hiddenUnits.push_back(new FermiUnit(2, weights));
			}
			
			l_hid = new PerceptronLayer(hiddenUnits);
			l_out = new PerceptronLayer(outputUnits);
			
			cout << "AVG ERR: " << testErrorVerbose() << endl;
			
			cout << "Training" << endl;
			for(int i = 0; i < 200; i++)
			{
				//cout << "Training with 500 random values." << endl;
				trainBothLayers(0.00001, 100);
				
				string filename = "TestResults";
				filename += to_string(i);
				filename += ".txt";
				
				ofstream outfile;
				outfile.open(filename);
				
				cout << "AVG ERR: " << testError() << endl;
				testErrorVerbose(outfile);
				
				outfile.close();
			}
			
			cout << endl << endl;
			cout << endl << "AVG ERR: " << testErrorVerbose() << endl;
		}

		double testError()
		{
			double avgErr = 0;
			double count = 0;
			
			//Werte durchgehen
			for(double x = -10; x <= 10.001; x+= 0.02)
			{
				std::vector<double> input;
				input.push_back(1); // Bias
				input.push_back(x);
				std::vector<double> o_hid = l_hid->getResult(input);
				
				//Bias
				o_hid.insert(o_hid.begin(),1);
				double o_out = l_out->getResult(o_hid)[0];
				double exp = -4*cos(x/3) + sin(15/(abs(0.5*x+2)+1))+0.2*x;
				
				double err = (exp - o_out)*(exp - o_out);
				
				count++;
				avgErr += err;
			}
			
			return avgErr/count;
		}

		double testErrorVerbose(std::ostream& outstream = std::cout)
		{
			double avgErr = 0;
			double count = 0;
			
			//outstream << "X\tRes\tExp\tErr" << std::endl << "-----------------------------" << std::endl;
			
			//Werte durchgehen
			for(double x = -10; x <= 10.001; x+= 0.02)
			{
				std::vector<double> input;
				input.push_back(1); // Bias
				input.push_back(x);
				std::vector<double> o_hid = l_hid->getResult(input);
				
				//Bias
				o_hid.insert(o_hid.begin(),1);
				double o_out = l_out->getResult(o_hid)[0];
				double exp = -4*cos(x/3) + sin(15/(abs(0.5*x+2)+1))+0.2*x;
				
				double err = (exp - o_out)*(exp - o_out);
				
				count++;
				avgErr += err;
				
				outstream << x << "\t" << o_out << "\t" << exp << "\t" << err << std::endl;
			}
			
			return avgErr/count;
		}
		
		void trainOutputLayer(double rate, int testSetCardinality)
		{
			try{
			//Generate x values
			list<double> testValues = chooseRandomXValues(testSetCardinality);
			int i = 0;
			cout << "Test vals:" << endl;			
			for(double testValue : testValues)
			{
				if(i++%16==0) cout << endl;
				cout << testValue << ", ";
			}
			cout << endl << endl;
			for(double testValue : testValues)
			{
				std::vector<double> input;
				input.push_back(1); // Bias
				input.push_back(testValue);
				
				std::vector<double> a_hid = l_hid->getIntegratedValues(input);
				std::vector<double> o_hid = l_hid->getTransferValues(a_hid);
				
				//Bias
				o_hid.insert(o_hid.begin(),1);
				
				std::vector<double> a_out = l_out->getIntegratedValues(o_hid);
				std::vector<double> o_out = l_out->getTransferValues(a_out);

				double exp = -4*cos(testValue/3) + sin(15/(abs(0.5*testValue+2)+1))+0.2*testValue;
				
				//sigm'(a) = sigm(a) * (1-sigm(a))
				cout << "delta_out=" << a_out[0] << "*(1-" << a_out[0] << ") * (" << o_out[0] << "-" << exp << ")";
				double delta_out = a_out[0] * (1-a_out[0])  *  (o_out[0] - exp);
				cout << "= " << delta_out << endl;
				
				//einzelne Gewichte anpassen

				for(int i = 0; i <= 10; i++)
				{
					outputUnits[0]->addToWeight(i, -rate * o_hid[i] * delta_out);
					cout << "Add to weight " << i << ":  -" << rate << " * " << o_hid[i] << "*" << delta_out << "=" << (-rate * o_hid[i] * delta_out) << endl;
				}
			}}
			catch(string e)
			{
				cout << e << endl;
			}
		}
		
		void trainBothLayers(double rate, int testSetCardinality)
		{
			try{
			//Generate x values
			list<double> testValues = chooseRandomXValues(testSetCardinality);
			int i = 0;
			/*cout << "Test vals:" << endl;			
			/for(double testValue : testValues)
			{
				if(i++%16==0) cout << endl;
				cout << testValue << ", ";
			}*/
			cout << endl << endl;
			for(double testValue : testValues)
			{
				cout << "XVAL = " << testValue << endl;
				std::vector<double> input;
				input.push_back(1); // Bias
				input.push_back(testValue);
				
				std::vector<double> a_hid = l_hid->getIntegratedValues(input);
				cout << "Integrated values Hid: ";
				for(double val : a_hid)
				{
					cout << val << ",";
				}
				std::vector<double> o_hid = l_hid->getTransferValues(a_hid);
				cout << endl << "Transfer values Hid: ";
				for(double val : o_hid)
				{
					cout << val << ",";
				}
				//Bias
				o_hid.insert(o_hid.begin(),1);
				
				std::vector<double> a_out = l_out->getIntegratedValues(o_hid);
				cout << endl << "Integrated values out: ";
				for(double val : a_out)
				{
					cout << val << ",";
				}
				std::vector<double> o_out = l_out->getTransferValues(a_out);
				cout << endl << "Transfer values out";
				for(double val : o_out)
				{
					cout << val << ",";
				}
				cout << endl;
				double exp = -4*cos(testValue/3) + sin(15/(abs(0.5*testValue+2)+1))+0.2*testValue;
				
				cout << "delta_out= 1 * (" << o_out[0] << "-" << exp << ")";
				double delta_out = 1 * (o_out[0] - exp);
				cout << "= " << delta_out << endl;
				
				std::vector<double> delta_hid;
				for(int i = 0; i < hiddenUnits.size(); i++)
				{
					//sigm'(a) = sigm(a) * (1-sigm(a))
					delta_hid.push_back(a_hid[i] * (1- a_hid[i]) * outputUnits[0]->getWeight(i+1) * delta_out);	//i+1 wegen Bias
				}


				//einzelne Gewichte der Output-Schicht anpassen
				for(int i = 0; i <= 10; i++)
				{
					outputUnits[0]->addToWeight(i, -rate * o_hid[i] * delta_out);
					cout << "Weight OUT-" << i << "   += " << (-rate * o_hid[i] * delta_out) << "=" << outputUnits[0]->getWeight(i) << endl;
				}

				//Gewichte der Hidden Schicht anpassen
				for(int i = 0; i < hiddenUnits.size(); i++)
				{
					for(int j = 0; j < 2; j++)
					{
						hiddenUnits[i]->addToWeight(j, -rate * input[j] * delta_hid[i]);
						cout << "Weight HID-" << i << "-" << j << " += " << (-rate * input[j] * delta_hid[i]) << "=" << hiddenUnits[i]->getWeight(j) << endl;
					}
				}
				cout << endl;
			}}
			catch(string e)
			{
				cout << e << endl;
			}
		}
		
};
