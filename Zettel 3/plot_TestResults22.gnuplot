set terminal png
set output 'trained.png'
set ylabel 'y'
set xlabel 'x'
set grid
plot 'TestResults22.txt' using 1:2 with lines lt 1 title "Ausgabe Netz", 'TestResults22.txt' using 1:3 with 
lines lt 2 
title "Echter Funktionswert", \
 'TestResults22.txt' using 1:4 with lines lt 3 title "Error^2"
