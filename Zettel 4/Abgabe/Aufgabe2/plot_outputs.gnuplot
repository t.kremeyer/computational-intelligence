set terminal png
set output 'outputs.png'
set ylabel 'y'
set xlabel 'x'
set grid
set pm3d map
set size square
set palette rgb 30,31,32
set cbrange [-1:1]
splot 'outputs.csv' using 1:2:3