g++ --std=c++17 -Wall *.cpp -o RBF
mkdir test
mkdir test/30UnitsSigma1
mkdir test/30UnitsSigma0.7
mkdir test/90UnitsSigma1
mkdir test/90UnitsSigma0.7
mkdir test/200UnitsSigma1
mkdir test/200UnitsSigma0.7
mkdir test/1000UnitsSigma1
mkdir test/1000UnitsSigma0.7
echo "Testing 30 Units, Sigma=1"
./RBF 30 0.1 10000 1
./plot_all.sh
mv centers.csv test/30UnitsSigma1/
mv outputs.csv test/30UnitsSigma1/
mv centers.png test/30UnitsSigma1/
mv outputs.png test/30UnitsSigma1/
echo "Testing 30 Units, Sigma=.7"
./RBF 30 0.1 10000 0.7
./plot_all.sh
mv centers.csv test/30UnitsSigma0.7/
mv outputs.csv test/30UnitsSigma0.7/
mv centers.png test/30UnitsSigma0.7/
mv outputs.png test/30UnitsSigma0.7/
echo "Testing 90 Units, Sigma=1"
./RBF 90 0.1 10000 1
./plot_all.sh
mv centers.csv test/90UnitsSigma1/
mv outputs.csv test/90UnitsSigma1/
mv centers.png test/90UnitsSigma1/
mv outputs.png test/90UnitsSigma1/
echo "Testing 30 Units, Sigma=.7"
./RBF 90 0.1 10000 0.7
./plot_all.sh
mv centers.csv test/90UnitsSigma0.7/
mv outputs.csv test/90UnitsSigma0.7/
mv centers.png test/90UnitsSigma0.7/
mv outputs.png test/90UnitsSigma0.7/
echo "Testing 200 Units, Sigma=1"
./RBF 200 0.1 10000 1
./plot_all.sh
mv centers.csv test/200UnitsSigma1/
mv outputs.csv test/200UnitsSigma1/
mv centers.png test/200UnitsSigma1/
mv outputs.png test/200UnitsSigma1/
echo "Testing 200 Units, Sigma=.7"
./RBF 200 0.1 10000 0.7
./plot_all.sh
mv centers.csv test/200UnitsSigma0.7/
mv outputs.csv test/200UnitsSigma0.7/
mv centers.png test/200UnitsSigma0.7/
mv outputs.png test/200UnitsSigma0.7/
echo "Testing 1000 Units, Sigma=1"
./RBF 1000 0.1 10000 1
./plot_all.sh
mv centers.csv test/1000UnitsSigma1/
mv outputs.csv test/1000UnitsSigma1/
mv centers.png test/1000UnitsSigma1/
mv outputs.png test/1000UnitsSigma1/
echo "Testing 1000 Units, Sigma=.7"
./RBF 1000 0.1 10000 0.7
./plot_all.sh
mv centers.csv test/1000UnitsSigma0.7/
mv outputs.csv test/1000UnitsSigma0.7/
mv centers.png test/1000UnitsSigma0.7/
mv outputs.png test/1000UnitsSigma0.7/
