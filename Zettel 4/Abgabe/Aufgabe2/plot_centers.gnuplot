set terminal png
set output 'centers.png'
set ylabel 'y'
set xlabel 'x'
set grid
set key outside center top
plot 'datasets.csv' using 2:3 with points lc rgb 'red' pt 5 ps 0.3 title 'Klasse 1, y=1', \
'datasets.csv' using 4:5 with points lc rgb 'blue' pt 7 ps 0.3 title 'Klasse 2, y=-1', \
 'centers.csv' using 1:2 with points lc rgb 'black' pt 9 ps 1 title 'Centers'