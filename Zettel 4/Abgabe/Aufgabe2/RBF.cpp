
#include <vector>
#include <algorithm>
#include <cmath>
#include <iostream>
#include <fstream>

using namespace std;


// **************************** Point generators ****************************
void class1(unsigned u, vector<double> &resultVector)
{
	if(u == 0) throw "u == 0";
	if(u > 100) throw "u > 100";

	resultVector.clear();
	resultVector.push_back(2+sin(0.2*u+8)*sqrt(u+10));
	resultVector.push_back(-1+cos(0.2*u+8)*sqrt(u+10));
}

void class2(unsigned u, vector<double> &resultVector)
{
	if(u == 0) throw "u == 0";
	if(u > 100) throw "u > 100";

	resultVector.clear();
	resultVector.push_back(2+sin(0.2*u-8)*sqrt(u+10));
	resultVector.push_back(-1+cos(0.2*u-8)*sqrt(u+10));
}

// **************************** class DataPointGenerator ****************************

class DataPointGenerator
{
public:
	bool done() const;
	void nextDataPoint(vector<double>& pointVector);
	void reset();
private:
	unsigned m_u = 1;
	bool m_class = 0;
};

bool DataPointGenerator::done() const
{
	if(m_u > 100) return true;
	return false;
}

void DataPointGenerator::nextDataPoint(vector<double>& pointVector)
{
	if(done()) throw "No more Data Points.";

	pointVector.clear();

	if(m_class == 0)
	{
		class2(m_u, pointVector);
		//Update Werte für nächsten Punkt
		m_class = 1;
	}
	else
	{
		class1(m_u, pointVector);
		//Update Werte für nächsten Punkt
		m_class = 0;
		m_u++;
	}
}

void DataPointGenerator::reset()
{
	m_u = 1;
	m_class = 0;
}

// **************************** struct RBFTopology ****************************
struct RBFTopology
{
    unsigned inputCount;
    unsigned rbfCount;
    unsigned outputCount;
	vector<double> minCenters;
	vector<double> maxCenters;
	double sigma = 0.8;
};

// **************************** interface Unit ****************************
class Unit
{
public:
    Unit(const unsigned inputCount, const vector<double> &minWeights, const vector<double> &maxWeights);
	Unit(const unsigned inputCount, const double minWeight = 0.0, const double maxWeight = 1.0);
    virtual void feedForward(const vector<double> &inputValues) = 0;
    virtual double getResult() const = 0;
	const vector<double>& getInputWeights() const;
protected:
    vector<double> m_inputWeights;
    double m_outputValue;
public:
    static double getRandom(double min, double max);
};

Unit::Unit(const unsigned inputCount, const vector<double> &minWeights, const vector<double> &maxWeights)
{
	if(minWeights.size() != inputCount) throw "Error constructing unit: Wrong minWeights size";
	if(maxWeights.size() != inputCount) throw "Error constructing unit: Wrong maxWeights size";

    m_outputValue = 0;
    for(unsigned i = 0; i < inputCount; i++)
    {
        m_inputWeights.push_back(getRandom(minWeights[i], maxWeights[i]));
    }
}

Unit::Unit(const unsigned inputCount, const double minWeight, const double maxWeight)
{
    m_outputValue = 0;
    for(unsigned i = 0; i < inputCount; i++)
    {
        m_inputWeights.push_back(getRandom(minWeight, maxWeight));
    }
}

double Unit::getRandom(const double min, const double max)
{
    double nextVal = rand() * ((max-min)/RAND_MAX);     //Scale: Rand between 0 and (max-min)
    nextVal += min;                                     //Move: Rand between min and min+(max-min) = max

    return nextVal;
}

const vector<double>& Unit::getInputWeights() const
{
	return m_inputWeights;
}

// **************************** class RBFUnit ****************************
class RBFUnit : public Unit
{
public:
    RBFUnit(const unsigned inputCount, const vector<double>& minWeights, const vector<double>& maxWeights) : Unit(inputCount, minWeights, maxWeights){}
	RBFUnit(const unsigned inputCount, const double minWeight = 0.0, const double maxWeight = 1.0) : Unit(inputCount, minWeight, maxWeight){}
	void setSigma(const double sigma){m_sigma = sigma;};
    void feedForward(const vector<double> &inputValues);
    double getResult() const;
	void setInputWeights(const vector<double>& inputWeights);
	static double integrate(const vector<double> &inputValues, const vector<double> &inputWeights);
    static double activate(const double input, const double sigma);
private:
	double m_sigma = 0.5;
};

void RBFUnit::feedForward(const vector<double> &inputValues)
{
    m_outputValue = integrate(inputValues, m_inputWeights);
}

double RBFUnit::getResult() const
{
    return activate(m_outputValue, m_sigma);
}

double RBFUnit::integrate(const vector<double> &inputValues, const vector<double> &inputWeights)
{
    if(inputValues.size() != inputWeights.size()) throw "Error on RBFUnit: integrate: input size wrong";

    double result = 0;

    for(unsigned i  = 0; i < inputValues.size(); i++)
    {
        result += ((inputValues[i]-inputWeights[i]) * (inputValues[i]-inputWeights[i]));
    }

    result = sqrt(result);

    return result;
}

double RBFUnit::activate(const double input, const double sigma)
{
    return exp
    (
        (-(input*input))
        /
        (2*sigma*sigma)
    );
}

void RBFUnit::setInputWeights(const vector<double>& inputWeights)
{
	if(inputWeights.size() != m_inputWeights.size()) throw "Error on RBFUnit::setInputWeights: Wrong number of weights";

	for(unsigned i = 0; i < m_inputWeights.size(); i++)
	{
		m_inputWeights[i] = inputWeights[i];
	}
}

// **************************** class LinearUnit ****************************
class LinearUnit : public Unit
{
public:
    LinearUnit(const unsigned inputCount, const vector<double> &minWeights, const vector<double> &maxWeights) : Unit(inputCount, minWeights, maxWeights){}
	LinearUnit(const unsigned inputCount, const double minWeight = 0.0, const double maxWeight = 1.0) : Unit(inputCount, minWeight, maxWeight){}
    void feedForward(const vector<double> &inputValues);
    double getResult() const;
	static double integrate(const vector<double> &inputValues, const vector<double>& inputWeights);
	static double activate(const double input);
	static double activateDiff(const double input);
	void deltaRule(double learningRate, double targetVal, vector<double> outputs);
};

void LinearUnit::feedForward(const vector<double> &inputValues)
{
    m_outputValue = integrate(inputValues, m_inputWeights);
}

double LinearUnit::getResult() const
{
    return activate(m_outputValue);
}

double LinearUnit::integrate(const vector<double> &inputValues, const vector<double>& inputWeights)
{
    if(inputValues.size() != inputWeights.size()) throw "Error on LinearUnit: integrate: input size wrong";

    double result = 0;

    for(unsigned i = 0; i < inputValues.size(); i++)
    {
        result += inputWeights[i] * inputValues[i];
    }

    return result;
}

double LinearUnit::activate(const double input)
{
    return input;
}

double LinearUnit::activateDiff(const double input)
{
    return 1;
}

void LinearUnit::deltaRule(double learningRate, double targetVal, vector<double> outputs)
{
    if(outputs.size() != m_inputWeights.size()) throw "Error on LinearUnit::deltaRule: Wrong number of output vals";

    for(unsigned i = 0; i < m_inputWeights.size(); i++)
    {
        m_inputWeights[i] += learningRate * (targetVal - m_outputValue) * outputs[i];
    }
}

// **************************** class FermiUnit ****************************
class ModifiedFermiUnit : public Unit
{
public:
    ModifiedFermiUnit(const unsigned inputCount, const vector<double> &minWeights, const vector<double> &maxWeights) : Unit(inputCount, minWeights, maxWeights){}
	ModifiedFermiUnit(const unsigned inputCount, const double minWeight = 0.0, const double maxWeight = 1.0) : Unit(inputCount, minWeight, maxWeight){}
    void feedForward(const vector<double> &inputValues);
    double getResult() const;
	static double integrate(const vector<double> &inputValues, const vector<double>& inputWeights);
	static double activate(const double input);
	static double activateDiff(const double input);
	void deltaRule(double learningRate, double targetVal, vector<double> outputs);
};

void ModifiedFermiUnit::feedForward(const vector<double> &inputValues)
{
    m_outputValue = integrate(inputValues, m_inputWeights);
}

double ModifiedFermiUnit::getResult() const
{
    return activate(m_outputValue);
}

double ModifiedFermiUnit::integrate(const vector<double> &inputValues, const vector<double>& inputWeights)
{
    if(inputValues.size() != inputWeights.size()) throw "Error on LinearUnit: integrate: input size wrong";

    double result = 0;

    for(unsigned i = 0; i < inputValues.size(); i++)
    {
        result += inputWeights[i] * inputValues[i];
    }

    return result;
}

double ModifiedFermiUnit::activate(const double input)
{
    return (2.0/(1.0+exp(-input)))-1;
}

double ModifiedFermiUnit::activateDiff(const double input)
{
    return 2*activate(input)*(1-activate(input));
}

void ModifiedFermiUnit::deltaRule(double learningRate, double targetVal, vector<double> outputs)
{
    if(outputs.size() != m_inputWeights.size()) throw "Error on LinearUnit::deltaRule: Wrong number of output vals";

    for(unsigned i = 0; i < m_inputWeights.size(); i++)
    {
        m_inputWeights[i] += learningRate * (targetVal - m_outputValue) * outputs[i];
    }
}

// **************************** class RBFNet ****************************
class RBFNet
{
public:
    RBFNet(const RBFTopology topology);
	void kMeans(DataPointGenerator& gen, unsigned iterations);
    void kMeansIteration(DataPointGenerator& gen);
    void feedForward(const vector<double> &inputValues);
    void deltaRule(const double learningRate, const vector<double> &expectedValues);
    void getResults(vector<double> &resultValues) const;
	const vector<RBFUnit>& getRBFUnits() const;
private:
    void getRBFOutputs(vector<double> &rbfOutputs) const;
	static double pointSquaredDistance(const vector<double>& p1, const vector<double>& p2);
    unsigned int m_inputSize;
    vector<double> m_input;
    vector<RBFUnit> m_rbfUnits;
    vector<ModifiedFermiUnit> m_outputUnits;
};

RBFNet::RBFNet(const RBFTopology topology)
{
    m_inputSize = topology.inputCount;
    //RBF Units
    for(unsigned i = 0; i < topology.rbfCount; i++)
    {
        m_rbfUnits.push_back(RBFUnit(topology.inputCount, topology.minCenters, topology.maxCenters));
		m_rbfUnits.back().setSigma(topology.sigma);
    }
    //Output Units
    for(unsigned i = 0; i < topology.outputCount; i++)
    {
        m_outputUnits.push_back(ModifiedFermiUnit(topology.rbfCount));
    }
}

void RBFNet::kMeans(DataPointGenerator& gen, unsigned iterations)
{
	for(unsigned i = 0; i < iterations; i++)
	{
		kMeansIteration(gen);
		gen.reset();
	}
}

void RBFNet::kMeansIteration(DataPointGenerator &gen)
{
	vector<vector<double>> centers;
	vector<vector<double>> newCenters;
	vector<unsigned> centerPointCount;

	//Initialize centers
	for(const RBFUnit &unit : m_rbfUnits)
	{
		centers.push_back(unit.getInputWeights());
		centerPointCount.push_back(0);
		newCenters.push_back(vector<double>());

		//Init newCenter to 0 so AVG can be calculated afterwards (as many 0s as dimensions in the actual weight vector)
		for(auto __attribute__((unused)) counter : unit.getInputWeights())
		{
			newCenters.back().push_back(0);
		}
	}

	//Iterate through points
	vector<double> point;
	vector<double> nearestCenter;
	double nearestCenterDist;
	unsigned nearestCenterIndex;

	for(gen.nextDataPoint(point); !gen.done(); gen.nextDataPoint(point))
	{
		nearestCenterDist = numeric_limits<double>::infinity();

		//Find nearest center
		for(unsigned i = 0; i < centers.size(); i++)
		{
			const vector<double>& center = centers[i];

			double dist = pointSquaredDistance(point, center);
			if(dist < nearestCenterDist)
			{
				nearestCenter = vector<double>(center);
				nearestCenterDist = dist;
				nearestCenterIndex = i;
			}
		}

		newCenters[nearestCenterIndex] = vector<double>(point);
		centerPointCount[nearestCenterIndex]++;
	}

	//Update centers
	for(unsigned i = 0; i < newCenters.size(); i++)
	{
		if(centerPointCount[i] <= 0) continue;

		for(double &center : newCenters[i])
		{
			center /= centerPointCount[i];
		}
		m_rbfUnits[i].setInputWeights(newCenters[i]);
	}
}

void RBFNet::deltaRule(const double learningRate, const vector<double> &expectedValues)
{
    vector<double> rbfOutputs;
    getRBFOutputs(rbfOutputs);

    for(unsigned i; i < m_outputUnits.size(); i++)
    {
        m_outputUnits[i].deltaRule(learningRate, expectedValues[i], rbfOutputs);
    }
}

void RBFNet::feedForward(const vector<double> &inputValues)
{
    if(inputValues.size() != m_inputSize) throw "Wrong number of input values";

    //Vector for the next layer
    vector<double> rbfOutput;

    //Feed forward through RBF Units
    for(RBFUnit &rbfUnit : m_rbfUnits)
    {
        rbfUnit.feedForward(inputValues);
        rbfOutput.push_back(rbfUnit.getResult());
    }

    //Feed forward through output Units
    for(ModifiedFermiUnit &outputUnit : m_outputUnits)
    {
        outputUnit.feedForward(rbfOutput);
    }
}

void RBFNet::getRBFOutputs(vector<double> &rbfOutputs) const
{
    rbfOutputs.clear();

    for(const RBFUnit &rbfUnit : m_rbfUnits)
    {
        rbfOutputs.push_back(rbfUnit.getResult());
    }
}

void RBFNet::getResults(vector<double> &resultValues) const
{
    resultValues.clear();

    for(const ModifiedFermiUnit &outputUnit : m_outputUnits)
    {
        resultValues.push_back(outputUnit.getResult());
    }
}

const vector<RBFUnit>& RBFNet::getRBFUnits() const
{
	return m_rbfUnits;
}

double RBFNet::pointSquaredDistance(const vector<double>& p1, const vector<double>& p2)
{
	if(p1.size() != p2.size()) throw "Error on RBFNet::pointDistance: Dimensions do not match";

	double dist = 0;

	for(unsigned i = 0; i < p1.size(); i++)
	{
		dist += (p1[i] - p2[i])*(p1[i]-p2[i]);
	}

	return dist;
}

// **************************** main ****************************

//min: inclusive; max: exclusive
double getRandomInt(const int min, const int max)
{
    double nextVal = rand() % (max-min);		//Scale: Rand between 0 and (max-min)
    nextVal += min;								//Move: Rand between min and min+(max-min) = max

    return nextVal;
}

double getAverageError(RBFNet &net)
{
	vector<double> inputVector;
	vector<double> resultsVector;
	double errorSum = 0;

	for(unsigned u = 1; u <= 100; u++)
	{
		class1(u, inputVector);
		net.feedForward(inputVector);
		net.getResults(resultsVector);

		errorSum += (1-resultsVector[0])*(1-resultsVector[0]);

		class2(u, inputVector);
		net.feedForward(inputVector);
		net.getResults(resultsVector);

		errorSum += (-1-resultsVector[0])*(-1-resultsVector[0]);
	}

	return errorSum/200;
}

void trainNet(RBFNet &net, double learningRate, unsigned exampleCount/*, std::ostream& outstream = std::cout*/)
{
	vector<double> inputVector;
	vector<double> expectedVector;
	expectedVector.push_back(0);

	//outstream << "Example\tAVG ERR" << endl;
	//outstream << "0\t" << getAverageError(net) << endl;

	for(unsigned i = 0; i < exampleCount; i++)
	{
		if(i % 1000 == 0) cout << "Training example: " << i << endl;

		if(getRandomInt(0, 2) == 0)
		{
			class1(getRandomInt(1, 101), inputVector);
			expectedVector[0] = 1;
			net.feedForward(inputVector);
			net.deltaRule(learningRate, expectedVector);
		}
		else
		{
			class2(getRandomInt(1, 101), inputVector);
			expectedVector[0] = -1;
			net.feedForward(inputVector);
			net.deltaRule(learningRate, expectedVector);
		}
		//outstream << i << "\t" << getAverageError(net) << endl;
	}
}

void printRBFCenters(const RBFNet &net, std::ostream& outstream = std::cout)
{
	const vector<RBFUnit>& rbfUnits = net.getRBFUnits();

	outstream << "X\tY" << endl;

	for(const RBFUnit &unit : rbfUnits)
	{
		const vector<double>& rbfWeights = unit.getInputWeights();

		for(double weight : rbfWeights)
		{
			outstream << weight << "\t";
		}
		outstream << endl;
	}
}

void printNetOutput(RBFNet& net, double xmin, double ymin, double xmax, double ymax, double xstep, double ystep, std::ostream& outstream = std::cout)
{
	vector<double> input;
	vector<double> output;

	outstream << "X\tY\tRes" << endl;

	for(double x = xmin; x < xmax; x += xstep)
	{
		for(double y = ymin; y < ymax; y += ystep)
		{
			input.clear();
			input.push_back(x);
			input.push_back(y);
			net.feedForward(input);
			net.getResults(output);
			outstream << x << "\t" << y << "\t" << output[0] << endl;
		}
		outstream << endl;
	}
}

int main(int argc, char *argv[])
{
    srand(time(0));

	unsigned hiddenUnitCount = argc >  1 ? atoi(argv[1]) : 30;
	double learningRate = argc > 2 ? atof(argv[2]) : 0.1;
	unsigned learningExampleCount = argc > 3 ? atoi(argv[3]) : 1000;
	double sigma = argc > 4 ? atof(argv[4]) : 0.7;

    RBFTopology topology = {2, hiddenUnitCount, 1};
	topology.minCenters.push_back(-10); //xmin
	topology.minCenters.push_back(-12); //ymin
	topology.maxCenters.push_back(13); //xmax
	topology.maxCenters.push_back(10); //ymax
	topology.sigma = sigma;

    vector<double> inputValues;
    vector<double> resultValues;

    RBFNet net(topology);

	DataPointGenerator gen;

	//net.kMeans(gen, 2);

	ofstream outfile;

	outfile.open("centers.csv");
	printRBFCenters(net, outfile);
	outfile.close();


	//outfile.open("error_development.csv");
    trainNet(net, learningRate, learningExampleCount);
	//outfile.close();

	outfile.open("outputs.csv");
	printNetOutput(net, -15, -15, 15, 15, 0.1, 0.1, outfile);
	outfile.close();
}
