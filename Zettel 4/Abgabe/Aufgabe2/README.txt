Das Skript execute.sh kompiliert das Programm automatisch mit g++, legt einen Ordner test und Unterordner f�r verschiedene Tests an, f�hrt das Programm f�r die Tests aus und legt die Testergebnisse in die passenden Ordner.

Dabei werden folgende Test-Cases durchgef�hrt:
Sigma: 0.7 oder 1
Unit-Anzahl in der RBF-Schicht: 30, 90, 200 oder 1000
#Trainingsdurchl�ufe (1 Durchlauf = 1 Wert): 10000
Lernrate: 0.1

Ergebnis: 
- Bei 30 Units ist deutlich zu sehen, dass nur an den wenigen Unit-Centern sinnvolle Werte rauskommen. Die Aufl�sung ist dadurch nicht besonders gro�
- Bei 90 Units w�chst die Dichte der Center schon an, bei einem Sigma-Wert von 1 sind die Spiralen ansatzweise zu erkennen, aber auch mit gro�en L�cken
- Wenn es noch mehr Units werden, wird das Ergebnis deutlich pr�ziser. Gerade bei 1000 Units ist die Aufl�sung so gro�, dass kleinere Sigma-Werte m�glich und an der Stelle auch passender werden. Hier ist auch der Bereich erkennbar, in dem sich �berhaupt RBF-Center befinden.

Technische Details:
-------------------
- datasets.csv: Rohdaten zum Plotten
- RBF.cpp: Enth�lt den ganzen Code, kompilierbar mit g++ mit dem C++17-Standard
	- Gibt aus:
	  centers.csv: Rohdaten & die zuf�llig gew�hlten Center der RBF-Units
	  outputs.csv: Ausgaben des Netzes f�r [-15,15][-15,15]
- plot_all.sh: Macht alle Plots

F�r die Visualisierung der Datenpunkte siehe die centers.png aus den Testergebnissen