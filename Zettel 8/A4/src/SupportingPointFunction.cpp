#include <vector>
#include <iostream>

using namespace std;


//A Function that has various uniform distributed supporting points and linear interpolations
class SupportingPointFunction : public vector<double>
{
private:
	double m_supPointMin;
	double m_supPointStep;

public:
	SupportingPointFunction(double supPointMin, double supPointStep, const vector<double> &supPointValues) : vector<double>(supPointValues), m_supPointMin(supPointMin), m_supPointStep(supPointStep)
	{}

	SupportingPointFunction(double supPointMin, double supPointStep) : vector<double>(), m_supPointMin(supPointMin), m_supPointStep(supPointStep)
	{}

	double operator()(double x)
	{
		if(x < m_supPointMin - 0.005) throw "x too low";
		else if (x > m_supPointMin + m_supPointStep * size() + 0.005) throw "x too high";

		//floor implicit
		unsigned index = (x - m_supPointMin) / m_supPointStep;

		//point is exactly the last sup. point -> Just return the value.
		if(index == size() - 1) return operator[](index);

		double leftX = (index * m_supPointStep) + m_supPointMin;
		double rightX = ((index + 1) * m_supPointStep) + m_supPointMin;

		//otherwise interpolate
		return operator[](index) + ((operator[](index+1) - operator[](index))/(rightX - leftX)) * (x - leftX);
	}
};

//Our function g(x)
class G : public SupportingPointFunction
{
public:
	G(const vector<double> &supPointValues) : SupportingPointFunction(-10,0.5,supPointValues){}
	G() : SupportingPointFunction(-10,0.5){}
};
