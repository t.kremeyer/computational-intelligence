#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
#include <random>

#include "SupportingPointFunction.cpp"

using namespace std;

double f(double x)
{
	return 3*cos(x/5.0) + sin(1.0/(2*abs(x)+0.25)) - 1;
}

//dump f(x) values to csv file
void printDatapoints(ostream &out = cout)
{
	out << "x\tf(x)" << endl;
	for(unsigned i = 0; i < 1001; i++)
	{
		double x = -10 + 0.02 * i;
		out << x << "\t" << f(x) << endl;
	}
}

//dump g(x) values to csv file
void printG(SupportingPointFunction &g, ostream &out = cout)
{
	out << "x\tg(x)" << endl;
	for(unsigned i = 0; i < 1001; i++)
	{
		double x = -10 + 0.02 * i;
		out << x << "\t" << g(x) << endl;
	}
}

//error function: Avg of sum((|f(x)-g(x)|+1)^2)
double gAvgError(SupportingPointFunction &g)
{
	double error = 0;
	for(unsigned i = 0; i < 1001; i++)
	{
		double x = -10 + 0.02 * i;
		double thisError = abs(f(x)-g(x));
		error += pow(thisError+1,2);
	}
	return error/1001;
}

//init support points to the values of f
void initSupPointValues(vector<double> &v)
{
	v.clear();
	for(unsigned i = 0; i < 41; i++)
	{
		double x = -10 + 0.5 * i;
		v.push_back(f(x));
	}
}

//create a mutation
//choose a support point to mutate randomly
//mutate it randomly (normal distributed)
void createMutation(G &child, const G &parent, normal_distribution<> &dist)
{
	static random_device rd;  //Will be used to obtain a seed for the random number engine
    static mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
	uniform_int_distribution<> uniform_dist(0, parent.size()-1); //Choose the parameter to change randomly

	child = parent;

	child[uniform_dist(gen)] += dist(gen);
}

//One evolution step with tournament selection between all childs
void evolveGStep(G &child, const G &parent, unsigned populationSize, unsigned turnierSize)
{
	static random_device rd;  //Will be used to obtain a seed for the random number engine
    static mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    uniform_int_distribution<> uniform_dist(0, populationSize-1);
	normal_distribution<> normal_dist(0,0.1);

	vector<G> childs;
	for(unsigned i = 0 ; i < populationSize; i++)
	{
		childs.push_back(G());
		createMutation(childs.back(), parent, normal_dist);
	}

	child.clear();

	double lastAvgError = numeric_limits<double>::max();

	for(unsigned i = 0; i < turnierSize; i++)
	{
		unsigned index = uniform_dist(gen);
		double avgError = gAvgError(childs[index]);
		if(avgError<lastAvgError)
		{
			child = childs[index];
			lastAvgError = avgError;
		}
	}
}

//whole evolution process
void evolveG(unsigned iterationCount)
{
	vector<double> supPointValues;
	initSupPointValues(supPointValues);

	G g(supPointValues);

	ofstream outfile;

	outfile.open("out/g_before.csv");
	printG(g, outfile);
	outfile.close();

	for(unsigned i = 0; i < iterationCount; i++)
	{
		evolveGStep(g, g, 100, 80);
		cout << "ERROR: " << gAvgError(g) << endl;
	}

	outfile.open("out/g_after.csv");
	printG(g, outfile);
	outfile.close();
}

int main()
{
	ofstream outfile;

	outfile.open("out/f.csv");
	printDatapoints(outfile);
	outfile.close();

	evolveG(100);
}
