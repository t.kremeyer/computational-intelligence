set terminal png
set output 'out/fg_before.png'
set ylabel 'y'
set xlabel 'x'
set grid
plot 'out/f.csv' using 1:2 with lines lc rgb 'red'  title 'f(x)', \
'out/g_before.csv' using 1:2 with lines lc rgb 'blue' title 'g(x)'
