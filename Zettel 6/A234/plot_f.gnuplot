set terminal png
set output 'out/f.png'
set ylabel 'y'
set xlabel 'x'
set grid
plot 'out/f.csv' using 1:2 with lines lc rgb 'red'  title 'f(x)'
