#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
#include <cstdarg>
#include <random>
#include <limits>

using namespace std;

class Function1D
{
public:
	virtual double operator()(double x) const = 0;
};

class Polynom : public Function1D
{
private:
	vector<double> m_coeff;

public:
	Polynom(const vector<double> &coeff)
	{
		for(const double &c : coeff)
		{
			m_coeff.push_back(c);
		}
	}

	double &operator[](unsigned i)
	{
		if(i >= m_coeff.size())
		{
			for(unsigned j = 0;  j < i - m_coeff.size() + 1; j++)
			{
				m_coeff.push_back(0);
			}
		}

		return m_coeff[i];
	}

	double operator()(double x) const
	{
		double result = 0;
		for(unsigned i = 0; i < m_coeff.size(); i++)
		{
			result += m_coeff[i]*pow(x,i);
		}
		return result;
	}

	unsigned degree() const
	{
		return m_coeff.size();
	}
};

class F : public Function1D
{
public:
	double operator()(double x) const
	{
		return -4*cos(x/3.0)+sin(15.0/(abs(0.5*x+2)+1))+0.2*x;
	}
};

double avgFuncDiff(const Function1D &p1, const Function1D &p2, double min, double step, unsigned count)
{
	double diff = 0.0;
	for(unsigned i = 0; i < count; i++)
	{
		double x = min + i * step;
		diff += abs(p1(x)-p2(x));
	}
	return diff / count;
}

void printF(ostream& outstream = std::cout)
{
	outstream << "x\tf(x)" << endl;
	F f;
	for(unsigned i = 0; i < 1001; i++)
	{
		outstream << 0.02*i-10 << "\t" << f(0.02*i-10) << endl;
	}
}

void printPolynom(const Polynom &p, ostream& outstream = std::cout)
{
	outstream << "x\tp(x)" << endl;
	for(unsigned i = 0; i < 1001; i++)
	{
		outstream << 0.02*i-10 << "\t" << p(0.02*i-10) << endl;
	}
}

void testCombinations(unsigned count = 1000, ostream& outstream = std::cout, ostream &info_outstream = std::cout)
{
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis(-3, 3);
	F f;
	vector<double> best;
	double bestError = std::numeric_limits<double>::max();

	for(unsigned i = 0; i < count; i++)
	{
		vector<double> v{dis(gen), dis(gen), dis(gen), dis(gen), dis(gen), dis(gen), dis(gen)};
		Polynom p(v);
		double error = avgFuncDiff(f, p, -10.0, 0.02, 1001);
		if(error < bestError)
		{
			bestError = error;
			best = v;
		}
	}

	info_outstream << endl << "TEST" << endl;
	info_outstream << "BEST ERROR: " << bestError << endl;
	info_outstream << "VALUES:" << endl;
	for(unsigned i = 0; i < best.size(); i++)
	{
		info_outstream << "a" << i << "=" << best[i] << endl;
	}

	printPolynom(Polynom(best), outstream);
}

void evolveValues(unsigned startCount = 100, unsigned incrementCount = 1000, ostream& outstream = std::cout, ostream &info_outstream = std::cout)
{
	std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<> dis1(-3, 3);
	std::uniform_real_distribution<> dis2(-0.1, 0.1);
	F f;
	vector<double> best;
	double bestError = std::numeric_limits<double>::max();

	for(unsigned i = 0; i < startCount; i++)
	{
		if((i%10)==0)
			cout << "RUN #" << i << endl;

		double prevError = std::numeric_limits<double>::max();

		vector<double> v{dis1(gen), dis1(gen), dis1(gen), dis1(gen), dis1(gen), dis1(gen), dis1(gen)};
		Polynom p(v);

		for(unsigned j = 0; j < incrementCount; j++)
		{
			vector<double> v2 = v;

			for(double &d : v2)
			{
				d+=dis2(gen);
			}

			Polynom p2(v2);

			double error = avgFuncDiff(f, p2, -10.0, 0.02, 1001);

			if(error < prevError)
			{
				prevError = error;
				p = p2;
				v = v2;
			}
		}

		if(prevError < bestError)
		{
			bestError = prevError;
			best = v;
		}
	}

	info_outstream << endl << "EVOLUTIONARY ALGO" << endl;
	info_outstream << "BEST ERROR: " << bestError << endl;
	info_outstream << "VALUES:" << endl;
	for(unsigned i = 0; i < best.size(); i++)
	{
		info_outstream << "a" << i << "=" << best[i] << endl;
	}

	printPolynom(Polynom(best), outstream);
}

int main()
{
	F f;
	Polynom g(vector<double>{-1/2.0, 1/4.0, -1/8.0, 1/16.0, -1/32.0, 1/64.0, -1/128.0});

	ofstream outfile_info;
	outfile_info.open("out/info.txt");

	ofstream outfile_csv;

	outfile_csv.open("out/f.csv");
	printF(outfile_csv);
	outfile_csv.close();

	outfile_csv.open("out/g.csv");
	printPolynom(g, outfile_csv);
	outfile_csv.close();

	outfile_info << "AVG Error: " << avgFuncDiff(f, g, -10.0, 0.02, 1001) << endl;

	outfile_csv.open("out/test_best.csv");
	testCombinations(1000, outfile_csv, outfile_info);
	outfile_csv.close();

	outfile_csv.open("out/evolutionary.csv");
	evolveValues(100, 1000, outfile_csv, outfile_info);
	outfile_csv.close();

	outfile_info.close();
}
