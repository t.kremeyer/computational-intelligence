set terminal png
set output 'datasets.png'
set ylabel 'y'
set xlabel 'x'
set grid
plot 'datasets.csv' using 2:3 with points lc rgb 'red' pt 5 title 'Klasse 1, y=1', \
'datasets.csv' using 4:5 with points lc rgb 'blue' pt 7 title 'Klasse 2, y=-1'